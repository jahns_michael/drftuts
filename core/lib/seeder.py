from core.structures.school.models.classroom import Major
from core.structures.school.models import Subject
from core.structures.school.constants import MAJOR_SUBJECT_MAPPING


def generate_subject_major():
    for (major, subjects) in MAJOR_SUBJECT_MAPPING.items():
        major_instance = Major.objects.filter(name=major).last()
        if not major_instance:
            major_instance = Major.objects.create(name=major)
            for subject in subjects:
                subject_instance = Subject.objects.filter(name=subject).last()
                if not subject_instance:
                    subject_instance = Subject.objects.create(name=subject)
                major_instance.subjects.add(subject_instance)
            major_instance.save()
    return Major.objects.all()
        
                
