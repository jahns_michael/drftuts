from django.db import models
from core.structures.common.models import Address

class Person(models.Model):
    full_name = models.CharField(max_length=50)
    email = models.EmailField(unique=True)
    identity_number = models.CharField(max_length=20)
    birthday = models.DateField()
    address = models.OneToOneField(Address, on_delete=models.CASCADE)