from django.db import models

class Address(models.Model):
    district = models.CharField(max_length=255)
    subdistrict = models.CharField(max_length=255)
    street_name = models.CharField(max_length=255)
    RT = models.CharField(max_length=4)
    RW = models.CharField(max_length=4)

    def __str__(self):
        return f'{street_name}, {RT}/{RW}, {subdistrict}-{district}'