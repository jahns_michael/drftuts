from django.db import models
from core.structures.person.models import Person
from core.structures.school.models.school import School
from core.structures.school import constants
from core.structures.school.models.subject import Subject

class SchoolResident(Person):
    school = models.ForeignKey(School, on_delete=models.CASCADE)

class Staff(SchoolResident):
    staff_number = models.CharField(max_length=30, unique=True)

class Student(SchoolResident):
    grade = models.PositiveIntegerField(choices=constants.GRADE_CHOICES)

class Teacher(SchoolResident):
    main_subject = models.ForeignKey(Subject, on_delete=models.CASCADE)
    subjects = models.ManyToManyField(Subject, related_name="%(app_label)s_%(class)s_subjects")