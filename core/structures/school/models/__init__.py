from .classroom import *
from .school import *
from .school_resident import *
from .score import *
from .subject import *