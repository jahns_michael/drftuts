from core.structures.common.models import Address
from django.db import models

class School(models.Model):
    name = models.CharField(max_length=30)
    address = models.OneToOneField(Address, on_delete=models.CASCADE)