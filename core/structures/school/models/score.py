from core.structures.school import constants
from core.structures.school.models.school_resident import Student
from core.structures.school.models.subject import Subject
from django.db import models

class Score(models.Model):
    student = models.OneToOneField(Student, on_delete=models.CASCADE)
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE)
    score = models.PositiveIntegerField()