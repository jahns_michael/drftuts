from core.structures.school.models.school import School
from django.db import models

class Subject(models.Model):
    name = models.CharField(max_length=20)