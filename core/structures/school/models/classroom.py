from core.structures.school.models.school import School
from core.structures.school.models.subject import Subject
from core.structures.school.models.school_resident import Teacher
from core.structures.school import constants
from django.db import models

class Major(models.Model):
    name = models.CharField(max_length=10)
    subjects = models.ManyToManyField(Subject, related_name="%(app_label)s_%(class)s_subjects")

class Classroom(models.Model):
    school = models.ForeignKey(School, on_delete=models.CASCADE)
    grade = models.PositiveIntegerField(choices=constants.GRADE_CHOICES)
    major = models.ForeignKey(Major, on_delete=models.CASCADE)
    homeroom_teacher = models.OneToOneField(Teacher, on_delete=models.CASCADE)