GRADE_CHOICES = (
    (1, "Grade One"),
    (2, "Grade Two"),
    (3, "Grade Three"),
)

MAJOR_SUBJECT_MAPPING = {
    "IPA": [
        "IPS",
        "Bahasa Indonesia",
        "Bahasa Inggris",
        "Olahraga",
        "Keterampilan",
        "Biologi",
        "Fisika",
        "Kimia",
        "Matematika Khusus",
    ],
    "IPS": [
        "IPA",
        "Bahasa Indonesia",
        "Bahasa Inggris",
        "Olahraga",
        "Keterampilan",
        "Matematika",
        "Sosiologi",
        "Geografi",
        "Ekonomi",
        "Sejarah",
    ],
    "Bahasa": [
        "IPA",
        "IPS",
        "Olahraga",
        "Keterampilan",
        "Bahasa Indonesia Khusus"
        "Bahasa Inggris Khusus"
        "Bahasa Mandarin"
        "Sastra dan Filsafat"
    ],
}