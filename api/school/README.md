# School Example

## Backlog

- [ ] CRUD School, All School Resident, Classroom
  - [ ] List Student, filter by Classroom
  - [ ] CRUD Student Score (by student ID)
  - [ ] Promote Student to next grade
- [ ] ReadOnly reference
  - [ ] Major
  - [ ] Subject
    - [ ] list Subject filtered by Major
  - [ ] Grade