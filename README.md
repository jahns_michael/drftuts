# DRF little tuts and cheetsheet

> NOTE that this is my own common practice, please consider it and customize it to your own liking


- [DRF little tuts and cheetsheet](#drf-little-tuts-and-cheetsheet)
  - [Setup](#setup)
    - [Clone](#clone)
    - [Venv](#venv)
    - [Install requirements](#install-requirements)
    - [Check](#check)
    - [Migrate](#migrate)
    - [Run](#run)
  - [In this repo](#in-this-repo)

## Setup

### Clone

```bash 
git clone https://gitlab.com/jahns_michael/drftuts.git
```

### Venv

```bash
python -m venv env
```

```bash
# Linux/MacOSX/UNIX-like
source ./env/bin/activate
```

```powershell
# Windows
.\env\Scripts\activate.bat
```

### Install requirements

```bash
python -m pip install -r requirements.txt
```

### Check

```bash
python manage.py check
```

### Migrate

```bash
python manage.py makemigrations
python manage.py migrate
```

### Run

```bash
python manage.py runserver
```

## In this repo

> To be continue